"""Dog modul"""


class Dog:
    """Dog class"""

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def sit(self):
        """Scenario_1 - dog sit on comand"""
        msg = self.name + " is sitting now"
        print(msg)
        return msg

    def jump(self):
        """Scenario_2 - dog jump on comand"""
        msg = self.name + " jumped"
        print(msg)
        return msg
