"""Test modul for Dog class"""

import unittest

from dog import Dog


class TestDog(unittest.TestCase):
    """Test class for Dog class"""

    def setUp(self):
        self.my_dog = Dog("Dzeki", 2)

    def test_should_confirm_that_dog_is_sit_down(self):
        """Testing Scenario_1, sitting comand for dog"""
        self.assertEqual(self.my_dog.name + " is sitting now", self.my_dog.sit())

    def test_should_confirm_that_dog_is_jump_up(self):
        """Testing Scenario_2, jumping comand for dog"""
        self.assertEqual(self.my_dog.name + " jumped", self.my_dog.jump())


unittest.main()
